"""
Definitions of Kinect SDK joint order and names
Author: Sebastian Nowozin <Sebastian.Nowozin@microsoft.com>
Adapted to python by Cheng-i Wang <chw160@ucsd.edu>
"""
import numpy as np
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.pyplot as plt
from matplotlib import animation as anime


NUI_SKELETON_POSITION_HIP_CENTER = 0
NUI_SKELETON_POSITION_SPINE = 1
NUI_SKELETON_POSITION_SHOULDER_CENTER = 2
NUI_SKELETON_POSITION_HEAD = 3
NUI_SKELETON_POSITION_SHOULDER_LEFT = 4
NUI_SKELETON_POSITION_ELBOW_LEFT = 5
NUI_SKELETON_POSITION_WRIST_LEFT = 6
NUI_SKELETON_POSITION_HAND_LEFT = 7
NUI_SKELETON_POSITION_SHOULDER_RIGHT = 8
NUI_SKELETON_POSITION_ELBOW_RIGHT = 9
NUI_SKELETON_POSITION_WRIST_RIGHT = 10
NUI_SKELETON_POSITION_HAND_RIGHT = 11
NUI_SKELETON_POSITION_HIP_LEFT = 12
NUI_SKELETON_POSITION_KNEE_LEFT = 13
NUI_SKELETON_POSITION_ANKLE_LEFT = 14
NUI_SKELETON_POSITION_FOOT_LEFT = 15
NUI_SKELETON_POSITION_HIP_RIGHT = 16
NUI_SKELETON_POSITION_KNEE_RIGHT = 17
NUI_SKELETON_POSITION_ANKLE_RIGHT = 18
NUI_SKELETON_POSITION_FOOT_RIGHT = 19

NUI_SKELETON_POSITION_COUNT = 20

HIP_CENTER = 0
SPINE = 1
SHOULDER_CENTER = 2
HEAD = 3
SHOULDER_LEFT = 4
ELBOW_LEFT = 5
WRIST_LEFT = 6
HAND_LEFT = 7
SHOULDER_RIGHT = 8
ELBOW_RIGHT = 9
WRIST_RIGHT = 10
HAND_RIGHT = 11
HIP_LEFT = 12
KNEE_LEFT = 13
ANKLE_LEFT = 14
FOOT_LEFT = 15
HIP_RIGHT = 16
KNEE_RIGHT = 17
ANKLE_RIGHT = 18
FOOT_RIGHT = 19

nui_skeleton_names = ['HIP_CENTER', 'SPINE', 'SHOULDER_CENTER', 'HEAD', 
                      'SHOULDER_LEFT', 'ELBOW_LEFT', 'WRIST_LEFT', 'HAND_LEFT', 
                      'SHOULDER_RIGHT', 'ELBOW_RIGHT', 'WRIST_RIGHT', 'HAND_RIGHT', 
                      'HIP_LEFT', 'KNEE_LEFT', 'ANKLE_LEFT', 'FOOT_LEFT', 
                      'HIP_RIGHT', 'KNEE_RIGHT', 'ANKLE_RIGHT', 'FOOT_RIGHT' ];

nui_skeleton_conn = [[HIP_CENTER, SPINE],
                     [SPINE, SHOULDER_CENTER],
                     [SHOULDER_CENTER, HEAD],
                     # Left arm 
                     [SHOULDER_CENTER, SHOULDER_LEFT],
                     [SHOULDER_LEFT, ELBOW_LEFT],
                     [ELBOW_LEFT, WRIST_LEFT],
                     [WRIST_LEFT, HAND_LEFT],
                     # Right arm ...
                     [SHOULDER_CENTER, SHOULDER_RIGHT],
                     [SHOULDER_RIGHT, ELBOW_RIGHT],
                     [ELBOW_RIGHT, WRIST_RIGHT],
                     [WRIST_RIGHT, HAND_RIGHT],
                     # Left leg ...
                     [HIP_CENTER, HIP_LEFT],
                     [HIP_LEFT, KNEE_LEFT],
                     [KNEE_LEFT, ANKLE_LEFT],
                     [ANKLE_LEFT, FOOT_LEFT],
                     # Right leg ...
                     [HIP_CENTER, HIP_RIGHT],
                     [HIP_RIGHT, KNEE_RIGHT],
                     [KNEE_RIGHT, ANKLE_RIGHT],
                     [ANKLE_RIGHT, FOOT_RIGHT]]

tagset = ['G1  lift outstretched arms', 'G2  Duck', 
          'G3  Push right', 'G4  Goggles', 'G5  Wind it up',
          'G6  Shoot', 'G7  Bow', 'G8  Throw', 'G9  Had enough',
          'G10 Change weapon', 'G11 Beat both', 'G12 Kick']

def load_file(filebasename):
    fdata = open(filebasename+'.csv', 'r')
    ftags = open(filebasename+'.tagstream','r')
    d = np.genfromtxt(fdata, delimiter = ' ')
    d = d.transpose()[1:].transpose()
    
    _n = np.where(np.sum(d,1) < 1.0e-10)[0]
    i = np.where(_n == np.arange(0,len(_n)))[0]
    i = np.max(i)
    d = d[i+1:]
    
    tag_1 = ftags.readline()
    tag_1 = tag_1.split(';')
    if tag_1[0] != 'XQPCTick' or tag_1[1][0:3] != 'Tag':
        print "Invalid tagstream file format"
        return   
    else:        
        t = np.genfromtxt(ftags, delimiter = ';',dtype = None)
    tick = np.array([_t[0] for _t in t])
    timestamp = (tick*1000 + 49875/2)/49875
    
    return d, tick, timestamp

def draw_skel(dd): 
    x = [data[0:-1:4] for data in dd]
    y = [data[1:-1:4] for data in dd]
    z = [data[2:-1:4] for data in dd]
    xmin = np.min(x); xmax = np.max(x)
    ymin = np.min(y); ymax = np.max(y)
    zmin = np.min(z); zmax = np.max(z)
    
    fig = plt.figure()
    ax = p3.Axes3D(fig)
    ax.set_xlim3d((xmin, xmax))
    ax.set_ylim3d((ymin, ymax))
    ax.set_zlim3d((zmin, zmax))
    ax.view_init(elev = -58, azim = 83)
    data = dd[0]
    s = np.reshape(data, (NUI_SKELETON_POSITION_COUNT,4)).transpose()
    lines = [ax.plot(s[0][c],s[1][c],s[2][c])[0] for c in nui_skeleton_conn]
    
    def update(ind, con_data, lines):
        data = con_data[ind]
        s = np.reshape(data, (NUI_SKELETON_POSITION_COUNT,4)).transpose()
        xvec = np.array([s[0][c] for c in nui_skeleton_conn])
        yvec = np.array([s[1][c] for c in nui_skeleton_conn])
        zvec = np.array([s[2][c] for c in nui_skeleton_conn])
        for i,line in enumerate(lines):        
            line.set_data(xvec[i],yvec[i])
            line.set_3d_properties(zvec[i])
            line.set_color('blue')
        return lines
    
    ani = anime.FuncAnimation(fig, update, fargs = (dd, lines),
                              frames = dd.shape[0], interval = 20)
#     ani.save('skeleton_test.mp4', fps=30)
#     plt.show()
    return(ani)
    
    
    
    
    